package Reflect;

public class ReflectionHelper {

  public double a = 20;
  private double b = 30;
  public double c = 40;

  public ReflectionHelper() {
  }

  public ReflectionHelper(double x, double y,double z) {
    this.a = x;
    this.b = y;
    this.c = z;
  }

  public void squareA() {
    this.a *= this.a;
  }

  public void squareB() {
    this.b *= this.b;
  }
  
  public void squareC() {
	    this.c *= this.c;
  }

  public double getA() {
    return a;
  }

  public void setA(double x) {
    this.a = x;
  }

  public double getB() {
    return b;
  }

  public void setB(double y) {
    this.b = y;
  }
  
  public double getC() {
	 return c;
	  }

  public void setC(double z) {
	 this.c = z;
	  }
  
  public String toString() {
    return String.format("(x:%f, y:%f, z:%f)", a, b, c);
  }
}
