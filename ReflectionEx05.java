package Reflect;

import java.lang.reflect.Field;

public class ReflectionEx05 {
	public static void main(String[] args) throws Exception {
		ReflectionHelper ref = new ReflectionHelper();
	    Field[] fields = ref.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", fields.length);

	    for (Field f : fields) {
	      System.out.printf("field name=%s type=%s value=%f\n", f.getName(),
	f.getType(), f.getDouble(ref));
	    }
	}
}



