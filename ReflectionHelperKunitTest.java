package Reflect;
import static Reflect.KunitTest.*;
public class ReflectionHelperKunitTest {
	public static void main(String[] args) {
		try {
			ReflectionHelper r = new ReflectionHelper(7,6,8);
			checkEquals(r.getA(),7);
			checkEquals(r.getB(),6);
			checkEquals(r.getC(),8);
			report();
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	}
	  void checkConstructorAndAccess(){
		  try {
			  ReflectionHelper r = new ReflectionHelper(5,7,9);
			    checkEquals(r.getA(), 7);
			    checkEquals(r.getB(), 7);
			    checkEquals(r.getC(), 7);
			    checkNotEquals(r.getA(), 7);    
			    checkNotEquals(r.getB(), 9); 
			    checkNotEquals(r.getC(), 9); 
		  } catch (ArithmeticException e) {
				System.out.println(e);
		  }
	  }	
	  void checkSquareA(){
		  try {
			  ReflectionHelper s = new ReflectionHelper(5,7,9);
			  s.squareA();
			  checkEquals(s.getA(), 11);
		  } catch (ArithmeticException e) {
				System.out.println(e);
		  }
	  }
	  void checkSquareB(){
		try {
			  ReflectionHelper s = new ReflectionHelper(5,7,9);
			  s.squareB();
			  checkEquals(s.getB(), 18);
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	  }
	  void checkSquareC(){
		  try {
			  ReflectionHelper s = new ReflectionHelper(5,7,9);
			  s.squareC();
			  checkEquals(s.getC(), 27);
		  } catch (ArithmeticException e) {
				System.out.println(e);
		  }
	  }
	  public static void main1(String[] args) {
		try {
			ReflectionHelperKunitTest test = new ReflectionHelperKunitTest();
		    test.checkConstructorAndAccess();
		    test.checkSquareA();
		    test.checkSquareB();
		    test.checkSquareC();
		    report();
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
		
	  }
}

