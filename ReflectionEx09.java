package Reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionEx09 {
	public static void main(String[] args) throws Exception {
		ReflectionHelper ref = new ReflectionHelper();
	    Method[] method = ref.getClass().getMethods();
	    System.out.printf("There are %d methods\n", method.length);

	    for (Method mtd : method) {
	      System.out.printf("method name=%s type=%s parameters = ", mtd.getName(),
	          mtd.getReturnType());
	      Class[] types = mtd.getParameterTypes();
	      for (Class cls : types) {
	        System.out.print(cls.getName() + " ");
	      }
	      System.out.println();
	    }
	  }
}