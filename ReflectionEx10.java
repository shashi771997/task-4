package Reflect;

import java.lang.reflect.*;

public class ReflectionEx10 {
	public static void main(String[] args) throws Exception {
		ReflectionHelper ref = new ReflectionHelper();
		Method method = ref.getClass().getDeclaredMethod("setA", double.class);
		method.setAccessible(true);
		method.invoke(ref, 86);
		System.out.println(ref);
	}

}

