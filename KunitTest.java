package Reflect;

import java.util.*;

public class KunitTest {
	private static List<String> checks;
	private static int checkscompleted = 0;
	private static int Checkspassed = 0;
	private static int Checksfailed = 0;
	
	private static void toReport(String txt) {
		try {
			if (checks == null) {
			      checks = new LinkedList<String>();
			    }
		checks.add(String.format("%04d: %s", checkscompleted++, txt));
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	  }
	public static void checkEquals(double val1, double val2) {
		try {
			if (val1 == val2) {
			      toReport(String.format("  %f == %f", val1, val2));
			      Checkspassed++;
			    } else {
			      toReport(String.format("* %f == %f", val1, val2));
			      Checksfailed++;
			    }
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	  }
	public static void checkNotEquals(double value1, double value2) {
		try {
			if (value1 != value2) {
			      toReport(String.format("  %fd != %f", value1, value2));
			      Checkspassed++;
			    } else {
			      toReport(String.format("* %f != %f", value1, value2));
			      Checksfailed++;
			    }
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	  }
	public static void report() {
		try {
			System.out.printf("%d checks passed\n", Checkspassed);
		    System.out.printf("%d checks failed\n", Checksfailed);
		    System.out.println();
		    
		    for (String check : checks) {
		      System.out.println(check);
		    }
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	  }
}

