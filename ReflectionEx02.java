package Reflect;

public class ReflectionEx02 {
	public static void main(String[] args) {
		ReflectionHelper ref = new ReflectionHelper();
	    ref.squareA();
	 // ref.squareB(); if uncomment gets compiler error
	    double a = ref.a;
	 // double b = ref.b; if uncomment gets compiler error   
	    System.out.println("square =" + ref);
	  }
}


