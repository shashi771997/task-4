package Reflect;

public class ReflectionEx03 {
	public static void main(String[] args) {
		ReflectionHelper ref = new ReflectionHelper();
		System.out.println("class = " + ref.getClass());
		System.out.println("class name = " + ref.getClass().getName());
	}
}