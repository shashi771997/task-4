package Reflect;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReflectionHelperTest {

	@Test
	public void test() {
		ReflectionHelper rf = new ReflectionHelper(5,10,15);
		
		rf.setC(30);
		
		double actual = rf.getC();
		double expected = 40;
		
		assertEquals(expected, actual, 0);
	}

}

