package Reflect;

import java.lang.reflect.Field;

public class ReflectionEx07 {
	  public static void main(String[] args) throws Exception {
		  ReflectionHelper ref = new ReflectionHelper();
		    Field[] fields = ref.getClass().getDeclaredFields();
		    System.out.printf("There are %d fields\n", fields.length);

		    for (Field field : fields) {
		    	field.setAccessible(true);
		      System.out.printf("field name=%s type=%s value=%f\n",
		    		  field.getName(), field.getType(), field.getDouble(ref));
		    }
	  }
}

